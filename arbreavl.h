/*  INF3105 - Structures de données et algorithmes       *
 *  Automne 2018 / ARBREAVL.H                            *
 *  Beaudoin-Thériault, Myrianne  BEAM26588114           */

#if !defined(__ARBREAVL_H__)
#define __ARBREAVL_H__

#include <assert.h>
#include <stdio.h>
#include <algorithm>
#include "pile.h"
#include <iostream>

template <class T>
class ArbreAVL {
  public:
    ArbreAVL();
    ArbreAVL(const ArbreAVL&);
    ~ArbreAVL();

    void inserer(const T&);
    bool contient(const T&) const;
    bool vide() const;
    void vider();
    void enlever(const T&);
    int  hauteur() const;
    
    class Iterateur; 
    Iterateur debut() const;
    Iterateur fin() const;
    Iterateur rechercher(const T&) const;
    Iterateur rechercherEgalOuSuivant(const T&) const;
    Iterateur rechercherEgalOuPrecedent(const T&) const;

    const T& operator[](const Iterateur&) const;
    T& operator[](const Iterateur&);
    ArbreAVL& operator = (const ArbreAVL&);

  private:
    struct Noeud{
        Noeud(const T&);
        T contenu;
        int equilibre;
        Noeud *gauche;
        Noeud *droite;
    };
    Noeud* racine;

	bool inserer(const T& element, Noeud*& noeud);
    bool contient(Noeud* noeud, const T& element) const;
	int hauteur(Noeud* noeud) const;
    void rotationGaucheDroite(Noeud*&);
    void rotationDroiteGauche(Noeud*&);
    void vider(Noeud*&);
    void copier(const Noeud*, Noeud*&) const;
    const T& max(Noeud*) const;
    bool enlever(const T& element, Noeud*& noeud);
    
  public:
    class Iterateur{
      public:
        Iterateur(const ArbreAVL& a);
        Iterateur(const Iterateur& a);
        Iterateur(const ArbreAVL& a, Noeud* c);
		Iterateur& operator++();
        Iterateur operator++(int);
        Iterateur& operator--();
        Iterateur operator--(int);
        Iterateur& operator = (const Iterateur&);
        
        operator bool() const;
        bool operator!() const;
        bool operator==(const Iterateur&) const;
        bool operator!=(const Iterateur&) const;
        const T& operator*() const;

      private:
        const ArbreAVL& arbre_associe;
        Noeud* courant;
        Pile<Noeud*> chemin;

      friend class ArbreAVL;
    };
};
//-----------------------------------------------------------------------------
template <class T>
ArbreAVL<T>::Noeud::Noeud(const T& c):
contenu(c), equilibre(0), gauche(NULL), droite(NULL){}

template <class T>
ArbreAVL<T>::ArbreAVL() : racine(NULL){}

template <class T>
ArbreAVL<T>::ArbreAVL(const ArbreAVL<T>& autre) : racine(NULL){ copier(autre.racine, racine);}

template <class T>
ArbreAVL<T>::~ArbreAVL() { vider();}

template <class T>
void ArbreAVL<T>::inserer(const T& element){ inserer(element, racine);}

template <class T>
bool ArbreAVL<T>::inserer(const T& element, Noeud*& noeud)
{
    if(noeud==NULL){
        noeud = new Noeud(element);
        return true;
    }
    if(element < noeud->contenu){
        if(inserer(element, noeud->gauche)){
            noeud->equilibre++;
            if(noeud->equilibre == 0) return false;
            if(noeud->equilibre == 1) return true;
            assert(noeud->equilibre==2);
            if(noeud->gauche->equilibre == -1)
                    rotationDroiteGauche(noeud->gauche);
                rotationGaucheDroite(noeud);
        }
        return false;
    }
    else if(noeud->contenu < element){
	    if(inserer(element, noeud->droite)){
            noeud->equilibre--;
            if(noeud->equilibre == 0) return false;
            if(noeud->equilibre == -1) return true;
            assert(noeud->equilibre== -2);
            if(noeud->droite->equilibre == 1)
                    rotationGaucheDroite(noeud->droite);
                rotationDroiteGauche(noeud);
        }
        return false;
    }
    noeud->contenu = element;
    return false;
  }

template <class T>
void ArbreAVL<T>::enlever(const T& element) { enlever(element, racine);}

template <class T>
bool ArbreAVL<T>::enlever(const T& element, Noeud*& noeud){
    if(element < noeud->contenu) {
	    bool retour = false;
        if(enlever(element, noeud->gauche)) {
            noeud->equilibre--;
    	    if(noeud->equilibre == 1) return false;
    	    if(noeud->equilibre == 0) return true;
            assert(noeud->droite!=NULL);
    	    retour = noeud->droite->equilibre != 0;
    	    if(noeud->droite->equilibre == 1)
                rotationGaucheDroite(noeud->droite);
    	    rotationDroiteGauche(noeud);
        }
	   return retour;
    }
    else if(element > noeud->contenu) {
        bool retour = false;
        if(enlever(element, noeud->droite)) {
            noeud->equilibre++;
            if(noeud->equilibre == -1) return false;
            if(noeud->equilibre == 0) return true;
            retour = noeud->gauche->equilibre != 0;
            if(noeud->gauche->equilibre == -1)
                rotationDroiteGauche(noeud->gauche);
            rotationGaucheDroite(noeud);
        }
        return retour;
    }
    Noeud* temp = noeud;
    if (noeud->gauche==NULL) {
        noeud = noeud->droite;
        delete temp;
        return true;
    }
    if (noeud->droite==NULL) {
        noeud = noeud->gauche;
        delete temp;
        return true;
    }
    noeud->contenu = max(noeud->gauche);
    noeud = temp;
    return enlever(noeud->contenu, noeud->gauche);
}

template <class T>
void ArbreAVL<T>::rotationGaucheDroite(Noeud*& racinesousarbre){
    Noeud *temp = racinesousarbre->gauche;
    int  ea = temp->equilibre;
    int  eb = racinesousarbre->equilibre;
    int  neb = -(ea>0 ? ea : 0) - 1 + eb;
    int  nea = ea + (neb < 0 ? neb : 0) - 1;
    temp->equilibre = nea;
    racinesousarbre->equilibre = neb;
    racinesousarbre->gauche = temp->droite;
    temp->droite = racinesousarbre;
    racinesousarbre = temp;
}

template <class T>
void ArbreAVL<T>::rotationDroiteGauche(Noeud*& racinesousarbre){
	Noeud *temp = racinesousarbre->droite;
    int  ea = temp->equilibre;
    int  eb = racinesousarbre->equilibre;
    int  neb = -(ea<0 ? ea : 0) + 1 + eb;
    int  nea = ea + (neb > 0 ? neb : 0) + 1;
    temp->equilibre = nea;
    racinesousarbre->equilibre = neb;
    racinesousarbre->droite = temp->gauche;
    temp->gauche = racinesousarbre;
    racinesousarbre = temp;
}

template <class T>
bool ArbreAVL<T>::vide() const{ return racine == NULL;}

template <class T>
void ArbreAVL<T>::vider(){
  vider(racine);
  racine = NULL;
}

template <class T>
void ArbreAVL<T>::vider(Noeud*& noeud){
    if (noeud == NULL) return;
    vider(noeud->gauche);
    vider(noeud->droite);
    delete noeud;
}

template <class T>
void ArbreAVL<T>::copier(const Noeud* source, Noeud*& noeud) const{
    if (source != NULL) {
        noeud = new Noeud(source->contenu);
        copier(source->gauche, noeud->gauche);
        copier(source->droite, noeud->droite);
    }
}

template <class T>
bool ArbreAVL<T>::contient(const T& element) const { return contient(racine, element);}

template <class T>
bool ArbreAVL<T>::contient(Noeud* noeud, const T& element) const{
    if(noeud == NULL) return false;
    if (noeud->contenu < element) return contient(noeud->droite, element);
    else if (noeud->contenu > element) return contient(noeud->gauche, element);
    else return true;
}

template <class T>
int  ArbreAVL<T>::hauteur() const{ return hauteur(racine);}

template <class T>
int  ArbreAVL<T>::hauteur(Noeud* noeud) const {
    if (noeud == NULL) return 0;
    return 1 + std::max(hauteur(noeud->gauche),hauteur(noeud->droite));
}

template <class T>
const T& ArbreAVL<T>::max(Noeud* n) const {
	while (n->droite != NULL) n = n->droite;
    return n->contenu;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::debut() const {
    Iterateur iter(*this);
    iter.courant = racine;
    if (iter.courant->gauche != NULL) {
        while(iter.courant->gauche != NULL) {
            iter.chemin.empiler(iter.courant);
            iter.courant = iter.courant->gauche;
        }
    }
    return iter;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::fin() const {return Iterateur(*this);}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercher(const T& e) const {
    Iterateur iter(*this);
    Noeud* n = racine;
    while(n){
        if (e < n->contenu) {
            iter.chemin.empiler(n);
            n = n->gauche;
        }
        else if (n->contenu < e) n = n->droite;
        else {
            iter.courant = n;
            return iter;
        }
    }
    iter.chemin.vider();
    return iter;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercherEgalOuSuivant(const T& e) const
{
    Iterateur iter(*this);
    Noeud* n = racine;
    while(n){
        if (e < n->contenu) {
            iter.chemin.empiler(n);
            n = n->gauche;
        }
        else if (n->contenu < e)  n = n->droite;
        else {
            iter.courant = n;
            return iter;
        }
    }
   if (!iter.chemin.vide()) {
   	iter.courant = iter.chemin.depiler();
   	while(iter && iter.courant->contenu < e) iter++;
   }
   return iter;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercherEgalOuPrecedent(const T& e) const{
    Noeud* n = racine, *dernier = NULL;
    while(n){
        if (e < n->contenu){
            n = n->gauche;
        }
        else if (n->contenu < e){
            dernier = n;
            n = n->droite;
        }
        else return rechercher(e);
    }
    if (dernier != NULL) return rechercher(dernier->contenu);
    return Iterateur(*this);
}

template <class T>
const T& ArbreAVL<T>::operator[](const Iterateur& iterateur) const{
    assert(&iterateur.arbre_associe == this);
    assert(iterateur.courant);
    return iterateur.courant->contenu;
}

template <class T>
T& ArbreAVL<T>::operator[](const Iterateur& iterateur){
    assert(&iterateur.arbre_associe == this);
    assert(iterateur.courant);
    return iterateur.courant->contenu;
}

template <class T>
ArbreAVL<T>& ArbreAVL<T>::operator=(const ArbreAVL& autre) {
    if(this==&autre) return *this;
    vider();
    copier(autre.racine, racine);
    return *this;
}

//-----------------------
template <class T>
ArbreAVL<T>::Iterateur::Iterateur(const ArbreAVL& a)
 : arbre_associe(a), courant(NULL){}

template <class T>
ArbreAVL<T>::Iterateur::Iterateur(const ArbreAVL<T>::Iterateur& a)
: arbre_associe(a.arbre_associe){
    courant = a.courant;
    chemin = a.chemin;
}

template <class T>
typename ArbreAVL<T>::Iterateur& ArbreAVL<T>::Iterateur::operator++(){
    assert(courant != NULL);
    Noeud* suivant = courant->droite;
    while(suivant!=NULL){
        chemin.empiler(suivant);
        suivant=suivant->gauche;
    }
    if (!chemin.vide()) courant = chemin.depiler();
    else courant = NULL;
    return *this;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::Iterateur::operator++(int){
    Iterateur copie(*this);
    operator++();
    return copie;
}

template <class T>
typename ArbreAVL<T>::Iterateur& ArbreAVL<T>::Iterateur::operator--(){
    assert(courant != NULL);
    Noeud* suivant = courant->gauche;
    while(suivant!=NULL){
        chemin.empiler(suivant);
        suivant=suivant->droite;
    }
    if (!chemin.vide()) courant = chemin.depiler();
    else courant = NULL;
    return *this;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::Iterateur::operator--(int){
    Iterateur copie(*this);
    operator--();
    return copie;
}

template <class T>
ArbreAVL<T>::Iterateur::operator bool() const{ return courant!=NULL;}

template <class T>
bool ArbreAVL<T>::Iterateur::operator!() const{
    return courant==NULL;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator==(const Iterateur& o) const{
    assert(&arbre_associe==&o.arbre_associe);
    return courant==o.courant;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator!=(const Iterateur& o) const{
    assert(&arbre_associe==&o.arbre_associe);
    return courant!=o.courant;
}

template <class T>
const T& ArbreAVL<T>::Iterateur::operator *() const{
    assert(courant!=NULL);
    return courant->contenu;
}

template <class T>
typename ArbreAVL<T>::Iterateur& ArbreAVL<T>::Iterateur::operator = (const Iterateur& autre){
    assert(&arbre_associe==&autre.arbre_associe);
    courant = autre.courant;
    chemin = autre.chemin;
    return *this;
}

#endif