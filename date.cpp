/*  INF3105 - Structures de données et algorithmes       *
 *  Automne 2018 / TP2                                   *
 *  Myrianne Beaudoin-Thériault	 BEAM26588114            */
 
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cmath>
#include "date.h"
//------------------------------------------------------- CONSTRUCTEUR ()
Date::Date(int j, int h, int m) {
	jours=j;
	heures=h;
	minutes=m;
}
//------------------------------------------------------- CONSTRUCTEUR PAR COPIE()
Date& Date::operator = (const Date& autre) {
    if(this==&autre) return *this;
    jours = autre.jours;
    heures = autre.heures;
    minutes = autre.minutes;
    return *this;
}
bool Date::operator < (const Date& d) const{
	if(jours < d.jours) return true;
	if(jours == d.jours && heures < d.heures) return true;
	if(jours == d.jours && heures == d.heures && minutes < d.minutes) return true;
    return false;
}
bool Date::operator > (const Date& d) const{
	if(jours > d.jours) return true;
	if(jours == d.jours && heures > d.heures) return true;
	if(jours == d.jours && heures == d.heures && minutes > d.minutes) return true;
    return false;
}
bool Date::operator >= (const Date& d) const{
	if(jours > d.jours) return true;
	if(jours == d.jours && heures > d.heures) return true;
	if(jours == d.jours && heures == d.heures && minutes > d.minutes) return true;
	if(jours == d.jours && heures == d.heures && minutes == d.minutes) return true;
    return false;
}
bool Date::operator == (const Date& d) const{
    return (jours == d.jours && heures == d.heures && minutes > d.minutes);
}
//------------------------------------------------------ ÉCRIRE LA DATE ()
std::ostream& operator << (std::ostream& os, const Date& d){
    char chaine[40];
    sprintf(chaine, "%dj_%02dh%02dm", d.jours, d.heures, d.minutes);
    os << chaine;
    return os;
}
//------------------------------------------------------ LIRE LA DATE()
std::istream& operator >> (std::istream& is, Date& d){
    char j, h, m, underscore;
    is >> d.jours >> j >> underscore >> d.heures >> h >> d.minutes >> m;
    assert(j=='j' && underscore=='_' && h=='h' && m=='m');
    return is;
}