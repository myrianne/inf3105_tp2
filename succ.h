/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP2                                   *
 *  Auteur(s):                                           *
 *                                                       *
 *                                                       */
#if !defined(__SUCC_H__)
#define __SUCC_H__
#include <string>
#include "arbreavl.h"
#include "arbremap.h"
#include "date.h"
#include "pointst.h"

using namespace std;

//------------------------------------------------------- CLASSE SUCCURSALE

class Succursale{
  public:
    Succursale() {}
    Succursale(std::string n, PointST p, int n_a, int n_p );
    Succursale(const Succursale& autre);
    std::string nom;
    PointST position;
    int nbPlaces;
    bool verifierDisposSuccUnique(Date debut, Date fin);
    bool verifierDisposEmprunt(Date debut);
    bool verifierDisposRetour(Date fin);
	void enregistrerEmprunt(Date debut);
	void enregistrerRetour(Date fin);    

 	
  private:
    ArbreMap<Date,int> agendaAutos;
  friend std::istream& operator >> (std::istream&, Succursale&);
  friend std::ostream& operator << (std::ostream&, const Succursale&);
};

#endif
