/*  INF3105 - Structures de données et algorithmes       *
 *  Automne 2018 / ARBREMAP.H                            *
 *  Beaudoin-Thériault, Myrianne  BEAM26588114           */

#if !defined(__ARBRE_MAP_H__)
#define __ARBRE_MAP_H__

#include "arbreavl.h"

template <class K, class V>
class ArbreMap
{
  private:
    class Entree
    {
      public:
        Entree(const K &c) : cle(c), valeur() {}
        K cle;
        V valeur;
        bool operator<(const Entree &e) const { return cle < e.cle; }
    };
    ArbreAVL < Entree > entrees;

  public:
    class Iterateur
    {
      public:
        Iterateur(const ArbreMap &a) : iter(a.entrees.debut()) {}
        Iterateur(typename ArbreAVL<Entree>::Iterateur i) : iter(i) {}
        operator bool() const { return iter.operator bool(); };
        Iterateur &operator++()
        {
            ++iter;
            return *this;
        }
        Iterateur &operator--()
        {
            --iter;
            return *this;
        }
        const K &cle() const { return (*iter).cle; }
        V &valeur() { return (V &)(*iter).valeur; }
        
      private:
        typename ArbreAVL<Entree>::Iterateur iter;
    };
    Iterateur debut() { return Iterateur(*this); }
    Iterateur fin() { return Iterateur(entrees.fin()); }
    Iterateur rechercher(const K &cle) { return Iterateur(entrees.rechercher(cle)); }
    Iterateur rechercherEgalOuSuivant(const K &cle)
    {
        return Iterateur(entrees.rechercherEgalOuSuivant(cle));
    }
    Iterateur rechercherEgalOuPrecedent(const K &cle)
    {
        return Iterateur(entrees.rechercherEgalOuPrecedent(cle));
    }

    bool contient(const K &) const;
    void enlever(K &);
    void vider();
    bool vide() const;
    bool vide();

    const V &operator[](const K &) const;
    V &operator[](const K &);
};

template <class K, class V>
void ArbreMap<K, V>::vider()
{
    entrees.vider();
}

template <class K, class V>
bool ArbreMap<K, V>::vide() const
{
    return entrees.vide();
}

template <class K, class V>
bool ArbreMap<K, V>::vide()
{
    return entrees.vide();
}

template <class K, class V>
void ArbreMap<K, V>::enlever(K &c)
{
    entrees.enlever(c);
}

template <class K, class V>
bool ArbreMap<K, V>::contient(const K &c) const
{
    return entrees.contient(c) != NULL;
}

template <class K, class V>
const V &ArbreMap<K, V>::operator[](const K &c) const
{
    typename ArbreAVL<Entree>::Iterateur iter = entrees.rechercher(c);
    return entrees[iter].valeur;
}

template <class K, class V>
V &ArbreMap<K, V>::operator[](const K &c)
{
    typename ArbreAVL<Entree>::Iterateur iter = entrees.rechercher(Entree(c));
    if (!iter)
    {
        entrees.inserer(Entree(c));
        iter = entrees.rechercher(c);
    }
    return entrees[iter].valeur;
}
template <class K, class V>
std::ostream &operator<<(std::ostream &os, const K &cle)
 {
     os << cle;
     return os;
 }

#endif