/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP2                                   *
 *  Beaudoin-Thériault, Myrianne  BEAM26588114           */
                                                        
#include "succ.h"
#include "date.h"
//------------------------------------------------------- CONSTRUCTEURS ()
Succursale::Succursale(std::string n, PointST p, int n_a, int n_p )
  : nom(n), position(p), nbPlaces(n_p + n_a)
{
	Date date(0,0,0);
	agendaAutos[date] = n_a;
}
Succursale::Succursale(const Succursale& autre)
  : nom(autre.nom), position(autre.position), nbPlaces(autre.nbPlaces)
{
	agendaAutos = autre.agendaAutos;
}
// --------------------------------------------------------- RESERVER SUCC UNIQUE()
void Succursale::reserverSuccUnique(Date debut, Date fin) {
	ArbreMap<Date,int>::Iterateur iterAutos = agendaAutos.rechercherEgalOuPrecedent(debut);
	bool ok = true;
	for(; ok && iterAutos && iterAutos.cle()<fin; ++iterAutos) if(iterAutos.valeur()==0) ok= false;	
	if(ok) {
		ArbreMap<Date,int>::Iterateur iterAutos = agendaAutos.rechercherEgalOuPrecedent(debut);
		if(iterAutos.cle() < debut) agendaAutos[debut] = iterAutos.valeur();
		ArbreMap<Date,int>::Iterateur iterPlaces = agendaAutos.rechercherEgalOuPrecedent(fin);
		if(iterPlaces.cle() < fin) agendaAutos[fin] = iterPlaces.valeur();
		for(iterAutos = agendaAutos.rechercher(debut); iterAutos.cle()<fin; ++iterAutos) agendaAutos[iterAutos.cle()] = --iterAutos.valeur();
		cout << "Acceptee" << endl;
	}
	else cout << "NonDispo" << endl;
}
// --------------------------------------------------------- VERIFIER DISPOS EMPRUNT()
bool Succursale::verifierDisposEmprunt(Date debut) {
	ArbreMap<Date,int>::Iterateur iterAutos = agendaAutos.rechercherEgalOuPrecedent(debut);
	for(; iterAutos; ++iterAutos) if(iterAutos.valeur() == 0) return false;	
	return true;
}
// --------------------------------------------------------- VERIFIER DISPOS RETOUR()
bool Succursale::verifierDisposRetour(Date fin) {
	ArbreMap<Date,int>::Iterateur iterPlaces = agendaAutos.rechercherEgalOuPrecedent(fin);
	for(; iterPlaces; ++iterPlaces) if((nbPlaces-iterPlaces.valeur()) == 0) return false;
	return true;
}
// --------------------------------------------------------- ENREGISTRER EMPRUNT()
void Succursale::enregistrerEmprunt(Date debut) {
	ArbreMap<Date,int>::Iterateur iterAutos = agendaAutos.rechercherEgalOuPrecedent(debut);
	if(iterAutos.cle() < debut) agendaAutos[debut] = iterAutos.valeur();
	for(iterAutos = agendaAutos.rechercher(debut); iterAutos; ++iterAutos) agendaAutos[iterAutos.cle()] = --iterAutos.valeur();
}
// --------------------------------------------------------- ENREGISTRER RETOUR()
void Succursale::enregistrerRetour(Date fin) {
	ArbreMap<Date,int>::Iterateur iterAutos = agendaAutos.rechercherEgalOuPrecedent(fin);
	if(iterAutos.cle() < fin) agendaAutos[fin] = iterAutos.valeur();
	for(iterAutos = agendaAutos.rechercher(fin);iterAutos; ++iterAutos) agendaAutos[iterAutos.cle()] = ++iterAutos.valeur();	
}
//------------------------------------------------------- LIRE UNE SUCCURSALE ()
std::istream& operator >> (std::istream& is, Succursale& s){
	char c;
	int n_a, n_p;
    is >> s.nom >> s.position >> n_a >> n_p >> c;
    assert(n_a > -1 && n_p > -1 );
    assert(c== ';');
    return is;
}
//------------------------------------------------------- ÉCRIRE UNE SUCCURSALE ()
std::ostream& operator << (std::ostream& os, const Succursale& s){
	Date date(0,0,0);
    os  <<  s.nom << '\t' << s.position << '\t' << s.nbPlaces << '\t' << std::endl;
    return os;
}