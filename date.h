/*  INF3105 - Structures de données et algorithmes       *
 *  Automne 2018 / TP2                                   *
 *  Beaudoin-Thériault, Myrianne	BEAM26588114		 */
 
#if !defined(__DATE__H__)
#define __DATE__H__
#include <iostream>

//------------------------------------------------------- CLASSE DATE

class Date{
  public:
    Date(){}
    Date(int j, int h, int m);
    Date& operator = (const Date& autre);
    
    bool operator < (const Date& d) const;
    bool operator > (const Date& d) const;
    bool operator >= (const Date& d) const;
    bool operator == (const Date& d) const;
  
  private:
    int jours;
    int heures;
    int minutes;
    
  friend std::ostream& operator << (std::ostream&, const Date& d);
  friend std::istream& operator >> (std::istream&, Date& d);
};

#endif