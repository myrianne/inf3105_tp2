/*  INF3105 - Structures de données et algorithmes       *
 *  Automne 2018 / TP2                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp2/              */ 
#include <fstream>
#include <iostream>
#include <string>
#include "succ.h"
#include "date.h"
#include "pointst.h"
#include "inventaire.h"
// --------------------------------------------------------- FXS()
int tp2(std::istream& entree);
bool verifierDispos(Date debut, Succursale origine, Date fin, Succursale destination);
bool verifierDisposEmprunt(Date debut, Succursale depart);
bool verifierDisposRetour(Date fin, Succursale destination);
bool verifierDisposSuccUnique(Date debut, Date fin, Succursale origine);

void enregistrerEmpruntRetour(Date debut, Date fin, string origine, ArbreMap<string,Succursale> &succursales);
void enregistrerEmprunt(Date debut, string origine, ArbreMap<string,Succursale> &succursales);
void enregistrerRetour(Date fin, string destination, ArbreMap<string,Succursale> &succursales);
int suggerer(PointST depart,const Date debut, PointST destination, Date fin, ArbreMap<string, Succursale> succursales);
// --------------------------------------------------------- MAIN()
int main(int argc, const char** argv){
    if(argc>1){
         std::ifstream entree_fichier(argv[1]);
         if(entree_fichier.fail()){
             std::cerr << "Erreur d'ouverture du fichier '" << argv[1] << "'" << std::endl;
             return 1;
         }
         return tp2(entree_fichier);
    }else
         return tp2(std::cin);        
    return 0;
}
// --------------------------------------------------------- TP2()
int tp2(std::istream& entree){
    ArbreMap<std::string, Succursale> succursales;
    Date debut, fin;
    PointST p, p_origine, p_destination;
    string commande, nom, origine, destination;
    int id=1, nbAutos=0, nbPlaces=0;
    char pointvirgule=0;
    while(entree){
        entree >> commande >> ws;
        if(!entree) break;
        cout << id << " : ";      
        if(commande=="creer"){
            entree >> nom >> p >> nbAutos >> nbPlaces;
            Succursale s(nom, p, nbAutos, nbPlaces);   
    		succursales[nom] = s; 
    		cout << "Creee" << endl;
        }else if(commande=="reserver"){
            entree >> origine >> debut >> destination >> fin;
            if(origine.compare(destination) == 0) enregistrerEmpruntRetour(debut, fin, origine, succursales);
			else if(verifierDispos(debut, succursales[origine], fin, succursales[destination])) {
				enregistrerEmprunt(debut, origine, succursales);
				enregistrerRetour(fin, destination, succursales);  
            	cout << "Acceptee" << endl;		
            }
            else cout << "NonDispo" << endl;
        }else if(commande=="suggerer"){
            entree >> p_origine >> debut >> p_destination >> fin;
            suggerer(p_origine, debut, p_destination, fin, succursales);
        }else{
            cout << "Commande '" << commande << "' invalide!" << endl;
            return 2;
        }
        entree >> pointvirgule >> ws;
        if(pointvirgule!=';'){
            cout << "Fichier d'entrée invalide!" << endl;
            return 3;
        }
        id++;
    }
    return 0;
}
// --------------------------------------------------------- VERIFIER DISPOS EMPRUNT RETOUR()
bool verifierDispos(Date debut, Succursale origine, Date fin, Succursale destination) {
	ArbreMap<Date,int>::Iterateur iterAutos = origine.agendaAutos.rechercherEgalOuPrecedent(debut);
	for(; iterAutos; ++iterAutos) if(iterAutos.valeur() == 0) return false;	
	ArbreMap<Date,int>::Iterateur iterPlaces = destination.agendaAutos.rechercherEgalOuPrecedent(fin);
	for(; iterPlaces; ++iterPlaces) if((destination.nbPlaces-iterPlaces.valeur()) == 0) return false;
	return true;
}
// --------------------------------------------------------- VERIFIER DISPOS EMPRUNT()
bool verifierDisposEmprunt(Date debut, Succursale depart) {
	ArbreMap<Date,int>::Iterateur iterAutos = depart.agendaAutos.rechercherEgalOuPrecedent(debut);
	for(; iterAutos; ++iterAutos) if(iterAutos.valeur() == 0) return false;	
	return true;
}
// --------------------------------------------------------- VERIFIER DISPOS RETOUR()
bool verifierDisposRetour(Date fin, Succursale destination) {
	ArbreMap<Date,int>::Iterateur iterPlaces = destination.agendaAutos.rechercherEgalOuPrecedent(fin);
	for(; iterPlaces; ++iterPlaces) if((destination.nbPlaces-iterPlaces.valeur()) == 0) return false;
	return true;
}
// --------------------------------------------------------- ENREGISTRER EMPRUNT RETOUR()
void enregistrerEmpruntRetour(Date debut, Date fin, string origine, ArbreMap<string,Succursale> &succursales) {
	Succursale succCopie = succursales[origine];
	ArbreMap<Date,int>::Iterateur iterAutos = succCopie.agendaAutos.rechercherEgalOuPrecedent(debut);
	ArbreMap<Date,int>::Iterateur iterPlaces = succCopie.agendaAutos.rechercherEgalOuPrecedent(fin);
	bool ok=true;
	if(iterAutos.cle() < debut) succCopie.agendaAutos[debut] = iterAutos.valeur();	
	if(iterPlaces.cle() < fin) succCopie.agendaAutos[fin] = iterPlaces.valeur();
	for(; iterAutos && iterAutos.cle()<fin; ++iterAutos) {
		if(iterAutos.valeur()==0) {
			cout << "NonDispo" << endl;
			ok = false;
			break;
		}
		if(iterAutos.cle() > debut) --iterAutos.valeur();	
	}
	if(ok) {
		succCopie.agendaAutos[debut]--; 
		&succursales[origine].agendaAutos = &succCopie.agendaAutos;
		cout << "Acceptee" << endl;
	}
}
// --------------------------------------------------------- ENREGISTRER EMPRUNT()
void enregistrerEmprunt(Date debut, string origine, ArbreMap<string,Succursale> &succursales) {
	ArbreMap<Date,int> agenda = succursales[origine].agendaAutos;
	ArbreMap<Date,int>::Iterateur iterAutos = agenda.rechercherEgalOuPrecedent(debut);
	if(iterAutos.cle() < debut) agenda[debut] = iterAutos.valeur();
	for(; iterAutos; ++iterAutos) if(iterAutos.cle() > debut) --iterAutos.valeur();	
	agenda[debut]--; 
	succursales[origine].agendaAutos = agenda;
}
// --------------------------------------------------------- ENREGISTRER EMPRUNT()
void enregistrerRetour(Date fin, string destination, ArbreMap<string,Succursale> &succursales) {
	ArbreMap<Date,int> agenda = succursales[destination].agendaAutos;
	ArbreMap<Date,int>::Iterateur iterAutos = agenda.rechercherEgalOuPrecedent(fin);
	if(iterAutos.cle() < fin) agenda[fin] = iterAutos.valeur();
	for(; iterAutos; ++iterAutos) if(iterAutos.cle() > fin) ++iterAutos.valeur();	
	agenda[fin]++;
	succursales[destination].agendaAutos = agenda;
}
// --------------------------------------------------------- SUGGERER()
int suggerer(PointST depart, Date debut, PointST destination, Date fin, ArbreMap<string, Succursale> succursales){
	ArbreMap<string, Succursale>::Iterateur iterSuccs = succursales.debut(), iterSuccs2 = succursales.debut();
	double d, d_min=distance(depart, iterSuccs.valeur().position);;
	string origine, arrivee, theOrigine, theArrivee;
	bool ok_depart = false, ok_dest = false, ok_depart_min = false, ok_dest_min = false;
	for(; iterSuccs; ++iterSuccs) {
		if(verifierDisposEmprunt(debut, iterSuccs.valeur())) { 
			d = distance(destination, iterSuccs.valeur().position);
			origine = iterSuccs.cle();
			ok_depart = true;
			if(d <= d_min ) {
				ok_depart_min = true;
				d_min = d;
				theOrigine = iterSuccs.cle();
			}
		}
	}
	d_min = distance(destination, iterSuccs2.valeur().position);
	for(; iterSuccs2; ++iterSuccs2) {
		if(verifierDisposRetour(fin, iterSuccs2.valeur())) {
			d = distance(destination, iterSuccs2.valeur().position);
			arrivee = iterSuccs2.cle();
			ok_dest = true;
			if(d <= d_min ) {
				ok_dest_min = true;
				d_min = d;
				theArrivee = iterSuccs2.cle();
			}
		}
	}
	if(ok_depart && ok_dest) cout << origine << '\t' << arrivee << endl;
	else if(ok_depart_min && ok_dest_min) cout << theOrigine << '\t' << theArrivee << endl;
	else cout << "Impossible" << endl;
	return 0;
}
