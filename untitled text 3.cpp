/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP2                                   *
 *  Beaudoin-Thériault, Myrianne                         *
 *                                                       */
#include "inventaire.h"

//------------------------------------------------------- CONSTRUCTEUR ()
Inventaire::Inventaire(int n_a, int n_p, )
  : nbAutos(n_a), nbPlaces(n_p)
{
	Inventaire inventaire(n_p, n_a);
}

//------------------------------------------------------- LIRE UNE SUCCURSALE ()
std::istream& operator >> (std::istream& is, Inventaire& i){
	
    is >> i.nbAutos >> i.nbPlaces;
    assert(i.nbAutos > -1 && i.nbPlaces > -1 );
    
    return is;
}

//------------------------------------------------------- ÉCRIRE UNE SUCCURSALE ()
std::ostream& operator << (std::ostream& os, const Inventaire& i){
    os  << i.nbAutos << '\t' << i.nbPlaces << '\t' << std::endl;
    
    return os;
}