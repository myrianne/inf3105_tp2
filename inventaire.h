/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP2                                   *
 *  Auteur(s):                                           *
 *                                                       *
 *                                                       */
#if !defined(__INVENTAIRE_H__)
#define __INVENTAIRE_H__
#include <string>
#include "arbreavl.h"
#include "arbremap.h"
#include "date.h"
#include "pointst.h"

//using namespace std;

//------------------------------------------------------- CLASSE SUCCURSALE

class Inventaire{
  public:
    Inventaire() {}
    Inventaire(int n_p, int n_a );
 	
  private:
  	int nbAutos;
    int nbPlaces;
    
  
  friend std::istream& operator >> (std::istream&, Succursale&);
  friend std::ostream& operator << (std::ostream&, const Succursale&);
};

#endif