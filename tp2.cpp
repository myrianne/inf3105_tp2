/*  INF3105 - Structures de données et algorithmes       *
 *  Automne 2018 / TP2                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp2/              */ 
#include <fstream>
#include <iostream>
#include <string>
#include "succ.h"
#include "date.h"
#include "pointst.h"
#include "inventaire.h"
// --------------------------------------------------------- FXS()
int tp2(std::istream& entree);
void suggerer(PointST depart,const Date debut, PointST destination, Date fin);
ArbreMap<std::string, Succursale> succursales;
// --------------------------------------------------------- MAIN()
int main(int argc, const char** argv){
    if(argc>1){
         std::ifstream entree_fichier(argv[1]);
         if(entree_fichier.fail()){
             std::cerr << "Erreur d'ouverture du fichier '" << argv[1] << "'" << std::endl;
             return 1;
         }
         return tp2(entree_fichier);
    }else
         return tp2(std::cin); 
    return 0;
}
// --------------------------------------------------------- TP2()
int tp2(std::istream& entree){
	
    string commande;
    int id=1;
    while(entree){
        entree >> commande >> ws;
        if(!entree) break;
        cout << id << " : ";  
            
        if(commande=="creer"){
        	PointST p;
        	int nbAutos, nbPlaces;
        	string nom;
            entree >> nom >> p >> nbAutos >> nbPlaces;
            Succursale s(nom, p, nbAutos, nbPlaces);   
    		succursales[nom] = s; 
    		cout << "Creee" << endl;
    		
        }else if(commande=="reserver"){
        	string nom, origine, destination;
        	Date debut, fin;
            entree >> origine >> debut >> destination >> fin;
            if(origine.compare(destination) == 0) succursales[origine].reserverSuccUnique(debut, fin);
			else {
				if(succursales[destination].verifierDisposRetour(fin) && succursales[origine].verifierDisposEmprunt(debut)) {
					succursales[origine].enregistrerEmprunt(debut);
					succursales[destination].enregistrerRetour(fin);  
            		cout << "Acceptee" << endl;		
            	}
            	else cout << "NonDispo" << endl;
            }
            
        }else if(commande=="suggerer"){
        	PointST p_origine, p_destination;
        	Date debut, fin;
            entree >> p_origine >> debut >> p_destination >> fin;
            suggerer(p_origine, debut, p_destination, fin);
            
        }else{
            cout << "Commande '" << commande << "' invalide!" << endl;
            return 2;
        }
        char pointvirgule=0;
        entree >> pointvirgule >> ws;
        if(pointvirgule!=';'){
            cout << "Fichier d'entrée invalide!" << endl;
            return 3;
        }
        id++;
    }
    return 0;
}
// --------------------------------------------------------- SUGGERER()
void suggerer(PointST depart, Date debut, PointST destination, Date fin){
	ArbreMap<string, Succursale>::Iterateur iterSuccs = succursales.debut(), iterSuccs2 = succursales.debut();
	double d, d_min=distance(depart, iterSuccs.valeur().position);;
	string origine, arrivee, theOrigine, theArrivee;
	bool ok_depart = false, ok_dest = false, ok_depart_min = false, ok_dest_min = false;
	for(; iterSuccs; ++iterSuccs) {
		if(succursales[iterSuccs.cle()].verifierDisposEmprunt(debut)) { 
			d = distance(destination, iterSuccs.valeur().position);
			origine = iterSuccs.cle();
			ok_depart = true;
			if(d <= d_min ) {
				ok_depart_min = true;
				d_min = d;
				theOrigine = iterSuccs.cle();
			}
		}
	}
	d_min = distance(destination, iterSuccs2.valeur().position);
	for(; iterSuccs2; ++iterSuccs2) {
		if(succursales[iterSuccs2.cle()].verifierDisposRetour(fin)) {
			d = distance(destination, iterSuccs2.valeur().position);
			arrivee = iterSuccs2.cle();
			ok_dest = true;
			if(d <= d_min ) {
				ok_dest_min = true;
				d_min = d;
				theArrivee = iterSuccs2.cle();
			}
		}
	}
	if(ok_depart && ok_dest) cout << origine << '\t' << arrivee << endl;
	else if(ok_depart_min && ok_dest_min) cout << theOrigine << '\t' << theArrivee << endl;
	else cout << "Impossible" << endl;
}
